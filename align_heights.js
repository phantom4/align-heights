
/**
 * 要素の高さを揃える
 * 
 * @author ngi@phantom4.org
 */
var AlignHeights;

AlignHeights = (function() {
  var _$elements, _config, _intervalTimer, _isPlaying, _numItem;

  _$elements = [];

  _numItem = 0;

  _intervalTimer = NaN;

  _isPlaying = false;

  _config = {};


  /**
   * constructor
   *
   * @param $elements {JQuery} 対象の要素
   * @param [option] {Object} オプション
   * @param [option.groupNum] {Int} 高さを揃えるグループの要素数
   * @param [option.offset] {Int} 高さを揃えた後に増減させる高さ
   * @param [option.interval] {Int} 処理を繰り返す間隔
   * @param [option.endless] {Boolean} 1度高さを揃え終わった後も監視する
   * @param [option.isSkipImageLoadError] {Boolean} 画像の読み込みエラーがあったら、高さ揃えを実行する
   * @param [option.isSkipSameHeight] {Boolean} 同じ高さならheightを設定しない
   * @param [option.complete] {Function}  コールバック
   */

  function AlignHeights($elements, option) {
    _$elements = $elements;
    _numItem = $elements.length;
    _config = $.extend({
      groupNum: _numItem,
      offset: 0,
      interval: 250,
      endless: false,
      skipImageLoadError: true,
      isSkipSameHeight: true,
      complete: null
    }, option);
    _intervalTimer = setTimeout((function(_this) {
      return function() {
        _this._alignHeight();
      };
    })(this), 1000);
    this._alignHeight();
    return this;
  }


  /**
   * 処理を停止する
   *
   */

  AlignHeights.prototype.stop = function() {
    if (_isPlaying === true && isNaN(_intervalTimer) === false) {
      clearInterval(_intervalTimer);
      _intervalTimer = NaN;
    }
  };


  /**
   * 処理中か
   *
   */

  AlignHeights.prototype.isPlaying = function() {
    return this.isPlaying;
  };


  /**
   * 要素の高さをまとめて揃える
   *
   */

  AlignHeights.prototype._alignHeight = function() {
    var group, isFinish, maxHeight, result;
    isFinish = true;
    group = [];
    maxHeight = 0;
    result = false;
    if (_$elements.length === 0) {
      isFinish = true;
    }
    _$elements.each((function(_this) {
      return function(index, element) {
        var $this, height;
        $this = $(element);
        group.push(element);
        height = Math.ceil($this.height());
        if (height > maxHeight) {
          maxHeight = height;
        }
        if (group.length >= _config.groupNum || index >= _numItem - 1) {
          if (maxHeight > 0) {
            result = _this._setTallestHeight(group, maxHeight + _config.offset, _config.skipImageLoadError, _config.isSkipSameHeight);
          }
          group = [];
          maxHeight = 0;
          if (result === false) {
            isFinish = false;
          } else {
            if (typeof _config.complete === "function") {
              _config.complete.apply();
            }
          }
        }
      };
    })(this));
    if (isFinish === true && _config.endless === false) {
      clearInterval(_intervalTimer);
      _intervalTimer = NaN;
      _isPlaying = false;
    }
  };


  /**
   * 要素の高さをまとめて揃える
   *
   * @param elements {Array} 要素
   * @param height {Number} 高さ
   * @param skipImageLoadError {Boolean} 画像の読み込みエラーを処理完了と判断する
   * @param isSkipSameHeight {Boolean} 要素の高さが同じなら高さを設定しない
   * @return {Boolean} 処理が完了したか
   */

  AlignHeights.prototype._setTallestHeight = function(elements, height, skipImageLoadError, isSkipSameHeight) {
    var element, elementHeight, i, images, img, index, index2, isSameHeight, j, k, len, len1, len2, len3, result;
    len = elements.length;
    isSameHeight = true;
    result = true;
    if (len > 0) {
      for (index = i = 0, len1 = elements.length; i < len1; index = ++i) {
        element = elements[index];
        images = $("img", element);
        if (images.length > 0) {
          for (index2 = j = 0, len2 = images.length; j < len2; index2 = ++j) {
            img = images[index2];
            if (img.complete !== true || (skipImageLoadError === false && img.height === 0)) {
              result = false;
              break;
            }
          }
        }
        if (isSkipSameHeight === true) {
          elementHeight = $(element).height();
          if (height !== elementHeight) {
            isSameHeight = false;
          }
        }
      }
    }
    if (result === true && (isSkipSameHeight === false || isSameHeight === false)) {
      for (index = k = 0, len3 = elements.length; k < len3; index = ++k) {
        element = elements[index];
        $(element).height(height);
      }
    }
    return result;
  };

  return AlignHeights;

})();
