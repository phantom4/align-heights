align_heights.js 0.1.0
============================
要素の高さを揃えるJavaScriptです。




仕様
----
* タイマーで高さ揃えを処理
* 対象要素内に画像が存在する場合、画像の読み込みを待って処理

 
使い方
------
### Step1: ファイルの読み込み

```html
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="js/align_heights.min.js"></script>
```

### Step2: マークアップ
```html
<ul class="hoge">
<li>1<br>2<br>3</li>
<li>A<br>B<br>C<br>D<br>E</li>
<li>a<br>b</li>
</ul>
```


### Step2: 呼び出し
```html
$(function () {
    var alignHeights = new AlignHeights($(".hoge li"));
});
```


メソッド
--------

### stop()
高さ揃えを停止する

```html
$(function () {
    var alignHeights = new AlignHeights($(".hoge li"));
    alignHeights.stop();
});
```

### isPlaying()
高さ揃えの処理が実行中か

@return {Boolean}

```html
$(function () {
    var alignHeights = new AlignHeights($(".hoge li"));
    alert(alignHeights.isPlaying());
});
```





パラメーター
-----------

### groupNum
要素を揃える個数

```
default: 要素全て
options: integer
```

```html
//例: 3つずつ揃える
$(function () {
    var alignHeights = new AlignHeights($(".hoge li"), {
        groupNum: 3
    });
});
```

### offset
取得した高さから増減させる高さ  
offset: 1 の設定で、取得した高さが50pxなら、51pxで設定される。  
ブラウザによっては取得した高さの小数点が丸められてしまい、実際の高さが取得できないことがあるとき、1を設定する

```
default: 0
options: integer
```


### interval
高さ合わせを実行する間隔（ミリ秒）

```
default: 250
options: integer
```


### endless
高さ揃えが完了した後も処理を続けるか

```
default: false
options: boolean (true or false)
```


### ignoreImageLoadError
対象要素内にある画像がリンク切れの場合、それを無視して高さ揃えを実行するか

```
default: true
options: boolean (true or false)
```


### isSkipSameHeight
グループ内の要素の高さが同じ場合（すべて揃っている場合）、高さ指定をしない

```
default: true
options: boolean (true or false)
```




今後の予定
----------
* completeメソッドを用意する？
* TypeScript版を作成する。


ライセンス
----------
Copyright &copy; 2015