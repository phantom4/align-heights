###*
 * 要素の高さを揃える
 * 
 * @author ngi@phantom4.org
###
class AlignHeights

  #----------------------------------
  #  elements
  #----------------------------------
  _$elements = []  #対象の要素

  #----------------------------------
  #  variables
  #----------------------------------
  _numItem = 0  #要素数
  _intervalTimer = NaN  #繰り返し用のタイマー
  _isPlaying = false  #高さ揃えを実行しているか
  _config = {}  #設定


  ###*
   * constructor
   *
   * @param $elements {JQuery} 対象の要素
   * @param [option] {Object} オプション
   * @param [option.groupNum] {Int} 高さを揃えるグループの要素数
   * @param [option.offset] {Int} 高さを揃えた後に増減させる高さ
   * @param [option.interval] {Int} 処理を繰り返す間隔
   * @param [option.endless] {Boolean} 1度高さを揃え終わった後も監視する
   * @param [option.isSkipImageLoadError] {Boolean} 画像の読み込みエラーがあったら、高さ揃えを実行する
   * @param [option.isSkipSameHeight] {Boolean} 同じ高さならheightを設定しない
   * @param [option.complete] {Function}  コールバック
  ###
  constructor: ($elements, option) ->
    _$elements = $elements
    _numItem = $elements.length

    #オプション値のマージ
    _config = $.extend({
      groupNum: _numItem
      offset: 0
      interval: 250
      endless: false
      skipImageLoadError: true
      isSkipSameHeight: true
      complete: null
    }, option)

    _intervalTimer = setTimeout(() =>
      @_alignHeight()
      return
    , 1000)
    @_alignHeight()

    return @


  ###*
   * 処理を停止する
   *
  ###
  stop: ->
    if _isPlaying == true && isNaN(_intervalTimer) == false
      clearInterval(_intervalTimer)
      _intervalTimer = NaN
    return

  ###*
   * 処理中か
   *
  ###
  isPlaying: ->
    return @isPlaying


  ###*
   * 要素の高さをまとめて揃える
   *
  ###
  _alignHeight: ->
    isFinish = true #終了したか
    group = []  #高さを揃える要素のリスト
    maxHeight = 0 #最大の高さ
    result = false  #処理結果

    if _$elements.length == 0
      isFinish = true

    _$elements.each((index, element) =>
      $this = $(element)
      group.push(element)  #要素をストック

      #一番高い高さを検出する
      height = Math.ceil($this.height()) #Firefox（それ以外のブラウザでも？）で小数点になることがあるので丸めておく
      if height> maxHeight
        maxHeight = height

      if group.length >= _config.groupNum || index >= _numItem - 1
        #規定要素数 or 要素が全部になったら
        if maxHeight > 0
          result = @_setTallestHeight(group, maxHeight + _config.offset, _config.skipImageLoadError, _config.isSkipSameHeight)

        #値をリセット
        group = []
        maxHeight = 0 #高さをクリア

        # すべての高さ揃えが完了していなければ、フラグを折っておく
        if result == false
          isFinish = false
        else
          #完了イベント
          if typeof _config.complete == "function"
            #TODO スコープをどこにするか？
            _config.complete.apply()

      return
    )

    # 全ての要素の高さ揃えが完了した？
    if isFinish == true && _config.endless == false
      clearInterval(_intervalTimer)
      _intervalTimer = NaN
      _isPlaying = false

    return


  ###*
   * 要素の高さをまとめて揃える
   *
   * @param elements {Array} 要素
   * @param height {Number} 高さ
   * @param skipImageLoadError {Boolean} 画像の読み込みエラーを処理完了と判断する
   * @param isSkipSameHeight {Boolean} 要素の高さが同じなら高さを設定しない
   * @return {Boolean} 処理が完了したか
  ###
  _setTallestHeight: (elements, height, skipImageLoadError, isSkipSameHeight) ->
    len = elements.length #要素数
    isSameHeight = true #高さが同じか
    result = true  #処理結果

    if len > 0
      for element, index in elements
        images = $("img", element) #要素内のimgタグを取得
        if images.length > 0
          for img, index2 in images
            if img.complete != true || (skipImageLoadError == false && img.height == 0)
              #画像の読み込みが完了していない。（画像読み込みエラーを無視しない場合は、それも考慮する）
              #→処理を中止
              result = false
              break

        #要素の高さが同じかを判別
        if isSkipSameHeight == true
          elementHeight = $(element).height()

          if height != elementHeight
            isSameHeight = false  #指定の高さと違うものがあれば、フラグを折る

    if result == true && (isSkipSameHeight == false || isSameHeight == false)
      #高さを揃える
      for element, index in elements
        $(element).height(height)

    return result